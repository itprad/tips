#!/bin/bash

# InstanceName(Name in Tag) is required.
if [ x"$1" == x ]; then
    echo "usage:"
    echo "  $0 {InstanceName}"
    exit 1;
fi
while [ $# -gt 0 ]
do

instance_name=$1
instanceid=$( aws ec2 describe-instances | jq ".Reservations[].Instances[] | select(.Tags[].Key==\"Name\" and .Tags[].Value==\"${instance_name}\").InstanceId" | sed 's/"//g' )

aws ec2 stop-instances --instance-ids ${instanceid}

shift
done
