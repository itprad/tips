#!/bin/bash

# InstanceName(Name in Tag) is required.
if [ x"$1" == x ]; then
    echo "usage:"
    echo "  $0 {InstanceName}"
    exit 1;
fi
while [ $# -gt 0 ]
do
instance_name=$1

global_ip=$( aws ec2 describe-instances | jq ".Reservations[].Instances[] | select(.Tags[].Key==\"Name\" and .Tags[].Value==\"${instance_name}\").PublicIpAddress" | sed 's/"//g' )

if [ x"${global_ip}" == x -o x"${global_ip}" == xnull ]; then
    echo "no EC2 instance named '${instance_name}' was found."
    echo "Or no Public IP was found for the instance."
    exit 1
fi

# replace the next line of
# "Host ${instance_name}
sed -i -e "/Host ${instance_name}/{n;s/^.*$/  HostName ${global_ip}/;}" ~/.ssh/config

echo "~/.ssh/config file has been updated with 'Host ${instance_name} = HostName ${global_ip}'"
shift
done
