# AWSのインスタンス名からGlobalIPを取得し、".ssh/config"を書き換えてくれるスクリプト

## 使い方

0 awsCLIを使えるようにする。

http://qiita.com/n0bisuke/items/1ea245318283fa118f4a

1 .ssh/configファイルを準備する。


```

Host <instance_name>
  HostName 13.112.102.48
    port 50000
    User Yoshiki.Akita
    IdentityFile ~/key/Yoshiki.Akita.pem
    ServerAliveInterval 60

```

↑のような記述を1インスタンス1セット作る。
<instance_name>には自分のインスタンス名/IdentityFileはSSH鍵の場所を指定する。
HostNameはスクリプト実行時にグローバルIPを補填してもらえますが、とりあえずはそれっぽい値を書いておくことをお勧めします。

2 update_ssh_config.shを実行する。

```

update_ssh_config.sh <instance_name> <instance_name2> ...

```

引数に起動しているinstance名を指定する。(複数指定可能)

3 sshでサーバーに接続する。

```

ssh <instance_name>

```

ご利用は自己責任で・・・

## 利用イメージ

```
ITPAdmin-no-MacBook-Pro:~ akita$ sh update_ssh_config.sh fab1
~/.ssh/config file has been updated with 'Host fab1 = HostName 54.92.51.140'
ITPAdmin-no-MacBook-Pro:~ akita$ ssh fab1
The authenticity of host '[54.92.51.140]:50000 ([54.92.51.140]:50000)' can't be established.
RSA key fingerprint is 78:8b:63:f4:de:50:81:66:67:0b:12:8d:37:98:3e:f9.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added '[54.92.51.140]:50000' (RSA) to the list of known hosts.
Welcome to Ubuntu 16.04.2 LTS (GNU/Linux 4.4.0-1017-aws x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  Get cloud support with Ubuntu Advantage Cloud Guest:
    http://www.ubuntu.com/business/services/cloud

22 packages can be updated.
0 updates are security updates.


Last login: Tue May 30 02:07:41 2017 from 111.239.61.139
Yoshiki.Akita@ip-172-31-20-117:~$

```